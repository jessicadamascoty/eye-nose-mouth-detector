# Introduction

This Object Detector Classifier detects the eyes, nose, and mouth of a person. The project makes use of TensorFlow to detect eyes, nose, and mouth. The implementation of this project largely follows the tutorials made by [EdjeElectronics](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10) and [sentdex](https://www.youtube.com/playlist?list=PLQVvvaa0QuDcNK5GeCQnxYnSSaar2tpku) whose documentation can be found in [pythonprogramming](https://pythonprogramming.net) which is linked in the description of his YouTube videos. 


I. Preparing TensorFlow
---
To install TensorFlow, the instructions can be found [here](https://www.tensorflow.org/install/). After validating the installation, you can test it, with the COCO dataset that it should come with. But before that, make sure to create a PYTHONPATH variable for \models, \models\research, and \models\research\slim by entering:

`set PYTHONPATH=C:\tensorflow1\models;C:\tensorflow1\models\research;C:\tensorflow1\models\research\slim`

We also need to compile [Protobuf](https://github.com/google/protobuf/releases) which can be done by running the following command in models\research\:

`protoc object_detection/protos/*.proto --python_out=.`

Finally, we need to run setup.py by running the following commands in models\research\:

`python setup.py build`
`python setup.py install`

After completing these, we can run object_detection_tutorial.ipynb which can be found in models/research/object_detection in Jupyter. This should output two labeled images in the end. If it is able to do this, TensorFlow is correctly installed and ready to train.

To test this using webcam feed with the COCO dataset, object_detection_webcam_coco.py can be run in IDLE. The following code was taken from [sentdex.](https://pythonprogramming.net/video-tensorflow-object-detection-api-tutorial/)


II. Data Preparation
---
The dataset is placed in models/research/object_detection. For my project, I named it Images. Each image found in this folder is manually labeled according to what you want to detect, which in my case is eye, nose, and mouth. 

This was done using [LabelImg](https://github.com/tzutalin/labelImg). This is an easy to use program where you select the directory of your images to label them. The following keyboard shortcuts were found the most useful:
- W: create a bounding box
- A: return to previous image
- D: go to the next image

After labeling each image, separate them into two new folders (test and train) inside the Images folder. Around 10% of your total dataset should be placed in the test folder and the rest will be placed in the train folder.

---

#### Turning the image labels into readable data for TensorFlow for training

The two following codes were taken from the [raccoon detector](https://github.com/datitran/raccoon_dataset):
- [xml_to_csv.py](https://github.com/datitran/raccoon_dataset/blob/master/xml_to_csv.py)
- [generate_tfrecord.py](https://github.com/datitran/raccoon_dataset/blob/master/generate_tfrecord.py)

The XML files made from using LabelImg should be turned into a CSV file, one each corresponding to the test and train folder. In case the names of your folders are not Images, test, and train, they can be easily edited in the following code: 

```
29    for folder in ['train','test']:
30        image_path = os.path.join(os.getcwd(), ('images/' + folder))
31        xml_df = xml_to_csv(image_path)
32        xml_df.to_csv(('images/' + folder + '_labels.csv'), index=None)
33        print('Successfully converted xml to csv.')
```

This should create test_labels.csv and train_labels.csv in the Images folder. To create data that is readable for TensorFlow to train, the CSV files has to be turned into *.record files. This is accomplished through generate_tfrecord.py. To use it, the labels need to be edited according to the labels of the custom objects that will be detected. 

```
30    def class_text_to_int(row_label):
31       if row_label == 'raccoon':
32           return 1
33       else:
34           None
```

In my eye, nose, and mouth detector, this code was edited to:

```
def class_text_to_int(row_label):
    if row_label == 'eye':
        return 1
    elif row_label == 'nose':
        return 2
    elif row_label == 'mouth':
        return 3
    else:
        None
```

With this, generate_tfrecord.py can be used by running the following commands to create test.record and train.record:

`python generate_tfrecord.py --csv_input=images\train_labels.csv --image_dir=images\train --output_path=train.record`

`python generate_tfrecord.py --csv_input=images\test_labels.csv --image_dir=images\test --output_path=test.record`

III. Training the model
---

To begin training, two files need to be obtained and placed in a new folder made in models\object_detection. In this case, this folder was named training and included:

#### Label Map
This is a *.pbtxt file that includes the labels and the ID of the objects that will be detected. The following format is followed: 

```
item {
  id: 1
  name: 'eye'
}

item {
  id: 2
  name: 'nose'
}

item {
  id: 3
  name: 'mouth'
}
```

#### Configuration
The *.config file will depend on the architecture that will be used. In this project, SSD with Mobilenet v1 was used, so the following files were downloaded:

- In models\object_detection: ssd_mobilenet_v1_coco_11_06_2017.tar.gz [download link](http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_11_06_2017.tar.gz)
- In models\object_detection\training: [ssd_mobilenet_v1_pets.config](https://raw.githubusercontent.com/tensorflow/models/master/object_detection/samples/configs/ssd_mobilenet_v1_pets.config)

---

#### Setting up the configuration

The following lines are based on ssd_mobilenet_v1_pets.config.
edit :

- **LINE 9** num_classes: 37 --> number of labels being classified
  - `num_classes: 3` (3 for eye, nose, mouth)
- **LINE 143** batch_size: 24 --> (depending on computer being used)

- **LINE 158** PATH_TO_BE_CONFIGURED/model.ckpt --> where model.ckpt of the architecture being used is 
  - `Tensorflow/models/research/object_detection/ssd_mobilenet_v1_coco_11_06_2017/model.ckpt`
- **LINE 172** PATH_TO_BE_CONFIGURED/pet_train.record --> where train.record is
  - `Tensorflow/models/research/object_detection/train.record`
- **LINE 183** PATH_TO_BE_CONFIGURED/pet_val.record --> where test.record is 
  - `Tensorflow/models/research/object_detection/test.record`
- **LINE 174 and 185** PATH_TO_BE_CONFIGURED/pet_label_map.pbtxt --> where labelmap.pbtxt is 
  - `Tensorflow/models/research/object_detection/training/labelmap.pbtxt`

---

#### Start the training

After creating the training folder and obtaining and editing the relevant files, the following line can be run to begin training:  
`python train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/ssd_mobilenet_v1_pets.config`

If you are using a different architecture, replace the  `training/ssd_mobilenet_v1_pets.config` after `--pipeline_config_path=` to where and what the .config file is

Ideally, training is done until the loss reaches 1 or less. To look at the progress of your training in graphical form, run the command:
`tensorboard --logdir=training`
This will create a link that will lead to tensorboard which shows loss and other graphs on the training. Once it reaches this, the process can be interrupted by CTRL+C.


#### Creating the inference graph
Enter the following line after replacing model.ckpt-XXXX with the highest ckpt file found in the training folder:
`python export_inference_graph.py --input_type image_tensor --pipeline_config_path training/faster_rcnn_inception_v2_pets.config --trained_checkpoint_prefix training/model.ckpt-XXXX --output_directory inference_graph`



IV. Adapting TensorFlow to analyze image, video, webcam feed
---
We can create a *.py file that can be run on IDLE to analyze either image, video, or webcam feed. These *.py files will be based on the object_detection_tutorial.ipynb file found in models\research\object_detection which is used to test TensorFlow using the COCO dataset. 

1. object_detection_image [[source]](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10)
  - line 33: `MODEL_NAME = 'inference_graph'` Needs to be changed if your model's inference graph is not named inference_graph
  - line 34: `IMAGE_NAME = 'test1.jpg'` Enter the file name of the image here

2. object detection_video [[source]](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10)
   - line 34: `MODEL_NAME = 'inference_graph'` Needs to be changed if your model's inference graph is not named inference_graph
   - line 35: `VIDEO_NAME = 'test.mov'` Enter the file name of the video here

3. object_detection_webcam [[source]](https://pythonprogramming.net/video-tensorflow-object-detection-api-tutorial/)
   - line 15: `cap = cv2.VideoCapture(1)` Needs to be changed to (0) if you are only using 1 webcam

---

#### What needed to be changed from the original object_detection_tutorial.ipynb file

Since we will now be testing the detector using our own model, the following code that refers to the model that will be used should be edited:
```
MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
```

**Replace this with:** `MODEL_NAME = 'INSERT_FILENAME_OF_MODEL_INFERENCE_GRAPH'`

Afterwards, delete the following code which downloads COCO:

```
opener = urllib.request.URLopener()
opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
tar_file = tarfile.open(MODEL_FILE)
for file in tar_file.getmembers():
  file_name = os.path.basename(file.name)
  if 'frozen_inference_graph.pb' in file_name:
    tar_file.extract(file, os.getcwd())
```



