These files are used to convert the ImageNet dataset into a usable format for TensorFlow training.

To use these, copy this to the models/research/object_detection folder 

I. xml_to_csv_imagenet.py
---
This code was copied from xml_to_csv.py with the slight change of switching the order of xmin,ymin,xmax,ymax. The XML files of imagenet follow the order of xmin,xmax,ymin,ymax. In order to keep the TensorFlow training consistent, the order that LabelImg uses, which is xmin,ymin,xmax,ymax, was used.

Since this follows the original training done, to use xml_to_csv_imagenet.py, we first need to separate the dataset being used into the train and test folder within the images folder in models/research/object_detection.

II. isolate_tags.py
---
This code was created to go through the test and train CSV files created after running xml_to_csv_imagenet.py. The two files that xml_to_csv_imagenet.py will generate are named test_labels.csv and train_labels.csv. To isolate a certain tag that will be used for object detection, the tag_to_isolate variable found in line 3 just needs to be changed. 

The output will be specific_test_labels.csv and specific_train_labels.csv. These CSV files will contain the rows of data that contain the tag found in tag_to_isolate obtained from test_labels.csv and train_labels.csv.

III. append_tags.py
---
To add more tags to specific_test_labels.csv and specific_train_labels.csv, append_tags.py is used. Similar to isolate_tags.py, to append the rows of data corresponding to a certain tag, the only variable that needs to be edited is tag_to_add which is found in line 3.

IV. convert_tags.py
---
Since ImageNet uses codes (such as n06874185) to represent the tags found in their dataset, convert_tags.py is used to convert the tags to their word equivalent. This is done using imagenet_tags.csv as its reference. It first obtains the column of codes and obtains each unique code used. Following this, each unique code is converted to its word equivalent after searching through imagenet_tags.csv. Once each word equivalent is found, the codes are replaced in specific_test_labels.csv and specific_test_labels.csv.

The output will be converted_test_labels.csv and converted_train_labels.csv. The third output will be labels_used.csv. This is used for write_label.py.

V. write_label.py
---
To create the TFrecords used for training, generate_tfrecord.py needs the labels that are used in the following format: 

```
def class_text_to_int(row_label):
    if row_label == 'eye':
        return 1
    elif row_label == 'nose':
        return 2
    elif row_label == 'mouth':
        return 3
    else:
        None
```

In order to avoid typing this down, in case there are multiple tags used, write_label.py creates a txt file named generate_tfrecord_labels.txt. This file obtains the tags used from labels_used.csv. The output will follow the necessary format so that it will only need to be copy-pasted onto generate_tfrecord.py.

The second output of write_label.py is the labelmap.pbtxt. This file is used for training and needs to follow the following format: 

```
item {
  id: 1
  name: 'eye'
}

item {
  id: 2
  name: 'nose'
}

item {
  id: 3
  name: 'mouth'
}
```

In order to avoid typing this down as well, write_label.py also uses labels_used.csv to create the pbtxt file on its own in the correct format already.



-----

Once the record files have been made, training can proceed as originally listed.
