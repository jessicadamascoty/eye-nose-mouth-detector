import csv

tag_to_isolate = 'n00007846' #edit tag here

with open ('images/test_labels.csv', 'r') as test_input, open('images/specific_test_labels.csv', 'w', newline = '') as test_output, open ('images/train_labels.csv', 'r') as train_input, open('images/specific_train_labels.csv', 'w', newline = '') as train_output: 
	test_reader = csv.reader(test_input)
	test_writer = csv.writer(test_output)
	train_reader = csv.reader(train_input)
	train_writer = csv.writer(train_output)

	header = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax'] 

	test_writer.writerow(header)
	train_writer.writerow(header) #write the header in specific_test-labels.csv

	for test_row in test_reader:
		if test_row[3] == tag_to_isolate: #checks 'class' column for specific label
			test_writer.writerow(test_row)

	for train_row in train_reader:
		if train_row[3] == tag_to_isolate: #checks 'class' column for specific label
			train_writer.writerow(train_row)

print('Successfully isolated.')