import csv

with open ('images/specific_test_labels.csv', 'r') as test_input, open('imagenet_tags.csv', 'r', newline = '') as labels_input, open ('images/specific_train_labels.csv', 'r') as train_input, open('images/converted_train_labels.csv', 'w', newline = '') as train_output, open('images/converted_test_labels.csv', 'w', newline = '') as test_output, open('images/labels_used.csv', 'w', newline = '') as labels_output:

	test_reader = csv.reader(test_input) #name test_input as test_reader
	train_reader = csv.reader(train_input)
	labels_reader = csv.reader(labels_input) #name labels_input as labels_reader

	test_writer = csv.writer(test_output) #name output as test_writer
	train_writer = csv.writer(train_output)
	label_writer = csv.writer(labels_output, delimiter = ',')
	
	next(test_reader) #ignore header row
	next(train_reader)

	header = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax'] 

	test_writer.writerow(header) #write the header in specific_test-labels.csv
	train_writer.writerow(header)

	test_imagenet_code = []
	train_imagenet_code = []
	rownum = 0

	for row in test_reader: #iterate through each row of specific_test_labels.csv
		test_imagenet_code.append(row[3])
		rownum += 1

	rownum = 0

	for row in train_reader: #iterate through each row of specific_test_labels.csv
		train_imagenet_code.append(row[3])
		rownum += 1

	test_input.seek(0)
	train_input.seek(0)

	#print("------> test_imagenet_code Array: {}".format(test_imagenet_code))

	unique = []
	[unique.append(item) for item in test_imagenet_code if item not in unique]

	#print("------> unique Array: {}".format(unique))

	num_unique = len(unique)
	#print("------> number of unique tags found in specific_test_labels.csv =  {}".format(num_unique))

	imagenet_label = []
	counter = 0

	while counter < num_unique:
		to_compare = unique[counter]
		for rowLabel in labels_reader: #iterate through each row of imagenet_tags.csv
			if rowLabel[0] == to_compare: #compare test_imagenet_code to each row in 1st column of imagenet_tags.csv
				label = rowLabel[1] #if it is true, set label as the word equivalent of the code
		imagenet_label.append(label)
		counter += 1
		labels_input.seek(0)

	#print("------> labels Array: {}".format(imagenet_label))

	for x in imagenet_label:
		label_writer.writerow([x])

	num_test_imagenet_code = len(test_imagenet_code)

	counter_code = 0
	index = 0
	replace_array = []

	while counter_code < num_test_imagenet_code:
		for code in unique:
			if test_imagenet_code[counter_code] == code:
				index = unique.index(test_imagenet_code[counter_code])
				replacement = imagenet_label[index]
				replace_array.append(replacement)
		counter_code += 1

	#print("------> replacement array: {}".format(replace_array))

	counter_replacement = 0

	next(test_reader)
	to_write = []

	while counter_replacement < num_test_imagenet_code:
		row = next(test_reader)
		row[3] = replace_array[counter_replacement]
		to_write.append(row)
		counter_replacement += 1

	test_writer.writerows(to_write)

	print("Successfully converted test tags")

	num_train_imagenet_code = len(train_imagenet_code)

	counter_code = 0
	index = 0
	replace_array = []

	while counter_code < num_train_imagenet_code:
		for code in unique:
			if train_imagenet_code[counter_code] == code:
				index = unique.index(train_imagenet_code[counter_code])
				replacement = imagenet_label[index]
				replace_array.append(replacement)
		counter_code += 1

	#print("------> replacement array: {}".format(replace_array))

	counter_replacement = 0

	next(train_reader)
	to_write = []

	while counter_replacement < num_train_imagenet_code:
		row = next(train_reader)
		row[3] = replace_array[counter_replacement]
		to_write.append(row)
		counter_replacement += 1

	train_writer.writerows(to_write)

	print("Successfully converted train tags.")

