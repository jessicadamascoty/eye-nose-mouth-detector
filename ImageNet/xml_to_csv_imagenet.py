import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET


def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text + ".jpeg",      #value in <filename> EDIT
                     int(root.find('size')[0].text),  #first value under <size> which should be width
                     int(root.find('size')[1].text),  #second value under <size> which should be height
                     member[0].text,                  #first value under object which is name (or class)
                     int(member[1][0].text),          #XMIN [1]second set of values under object which is found in <bndbox> 
                     int(member[1][2].text),          #YMIN
                     int(member[1][1].text),          #XMAX
                     int(member[1][3].text)           #YMAX
                     ) 
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax'] 
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


def main():
    for folder in ['test','train']: #TO RENAME: if folders of dataset are named differently
        image_path = os.path.join(os.getcwd(), ('images/' + folder))
        xml_df = xml_to_csv(image_path)
        xml_df.to_csv(('images/' + folder + '_labels.csv'), index=None)
        print('Successfully converted xml to csv.')


main()
