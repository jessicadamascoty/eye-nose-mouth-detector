import csv

tag_to_add = 'n06874185' #edit tag here

with open("images/specific_train_labels.csv", 'a', newline = '') as append_train, open ('images/train_labels.csv', 'r') as input_train, open("images/specific_test_labels.csv", 'a', newline = '') as append_test, open ('images/test_labels.csv', 'r') as input_test:
	train_reader = csv.reader(input_train)
	test_reader = csv.reader(input_test)
	train_append = csv.writer(append_train)
	test_append = csv.writer(append_test)

	for train_row in train_reader:
		if train_row[3] == tag_to_add: #checks 'class' column for specific label
			train_append.writerow(train_row)

	for test_row in test_reader:
		if test_row[3] == tag_to_add: #checks 'class' column for specific label
			test_append.writerow(test_row)

print('Successfully appended.')
