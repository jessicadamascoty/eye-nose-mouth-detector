import csv

tfrecord_file = open('images/generate_tfrecord_labels.txt', 'w')
labelmap_file = open('images/labelmap.pbtxt', 'w')


with open ('images/labels_used.csv', 'r') as inputCSV:
	reader = csv.reader(inputCSV)

	labels = []
	rownum = 0

	for row in reader: #iterate through each row of specific_test_labels.csv
		labels.append(row[0])
		rownum += 1

	print('Labels: {}'.format(labels))

	num_labels = len(labels)
	num_iteration = num_labels - 1

	counter = 1
	return_val = 2
	flag = 0

	tfrecord_file.write("if row_label == \'" + labels[0] + "\': \n")
	tfrecord_file.write("\t return 1 \n")


	while flag < num_iteration:
		tfrecord_file.write("elif row_label == \'" + labels[counter] + "\': \n")
		tfrecord_file.write("\t return {}".format(return_val) + "\n")
		counter += 1
		return_val += 1
		flag += 1

	tfrecord_file.write("else: \n")
	tfrecord_file.write("\t None")

	tfrecord_file.close()

	print("Successfully written generate_tfrecord_labels.txt")

	flag = 0
	id_val = 1

	while flag < num_labels:
		labelmap_file.write("item { \n")
		labelmap_file.write("\tid: {}".format(id_val) + "\n")
		labelmap_file.write("\tname: \'" + labels[flag] + "\' \n")
		labelmap_file.write("}\n \n")
		flag += 1
		id_val += 1

	labelmap_file.close()

	print("Successfully written labelmap.pbtxt")


